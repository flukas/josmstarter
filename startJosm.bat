rem @echo off
rem default settings

set downloadFileVersion=tested
set runTracerServer=false

rem read parameters
:Loop
IF "%1"=="" GOTO Continue

    echo %1
    GOTO %1

    :/latest
        set downloadFileVersion=latest
        goto :END_SWITCH

    :/noupdate
        goto runapps

    :/notracer
    set runTracerServer=false
        goto :END_SWITCH

	:/tracer
    set runTracerServer=true
        goto :END_SWITCH
    :/help
        echo:JOSM starter script
        echo:starts JOSM tested or latest version, updates the program if needed. 
        echo:keeps all settings in the directory from where it was started
        echo:run [/latest] [/notracer] [/help] [/noupdate]
        echo:    /latest    runs latest version instead of tested
        echo:    /notracer  does not run tracer server
		echo:    /tracer	runs the tracer server	
        echo:    /help  displays this help
        echo:    /noupdate  does not run update, any parameters after this one are ignored
        goto end

    :/?
    goto /help

    :-?
        goto /help

    :-help
        goto /help

    GOTO END_SWITCH
    :END_SWITCH
SHIFT
GOTO Loop
:Continue

echo:Setting up settings + plugin location
set APPDATA=appdata
echo:set to %APPDATA%

echo:Determining actual %downloadFileVersion% version number (server)
curl https://josm.openstreetmap.de/%downloadFileVersion% -verbose -o %downloadFileVersion%.txt
set /p serverVersion=<%downloadFileVersion%.txt

echo:Determining current version
rem https://regexr.com/
java.exe -Xmx1024M -jar josm-%downloadFileVersion%.jar --version | findstr /R:"(?<=[(])([0-9])*(?=.*[)])" > current.txt
set /p currentVersion=<current.txt
rem set currentVersion=%temp:~10,4%

echo Server %downloadFileVersion%: %serverVersion%
echo Local %downloadFileVersion%: %currentVersion%

set /a diffVersion=serverVersion-currentVersion
echo Difference: %diffVersion%

if %diffVersion% == 0 (
    echo:JOSM is up to date 
    goto runapps
) else (
    echo:Update or downgrade needed
)

echo:Downloading latest version
rem Download to temporary file, then copy and remove - to prevent file corruption when download fails
curl https://josm.openstreetmap.de/download/josm-%downloadFileVersion%.jar -o josm-%downloadFileVersion%-download.jar
rem todo - check download success
move /Y josm-%downloadFileVersion%-download.jar josm-%downloadFileVersion%.jar
del josm-%downloadFileVersion%-download.jar

:runapps

echo:Starting JOSM 
start java.exe -Djava.net.useSystemProxies=true -Xmx1024M -jar josm-%downloadFileVersion%.jar

if %runTracerServer% == true (
	rem for download here: https://sourceforge.net/projects/tracer2server/files/latest/download
    echo:Starting tracer server
    start TracerServer\bin\Osm.Kn.Trace.Server.exe
)

rem pause

rem Delete temporary files
rem del %downloadFileVersion%.txt
rem del current.txt

:end
